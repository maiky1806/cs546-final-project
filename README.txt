README.txt
********************************************************************

Final Programming Project - CS546 Parallel & Distributed processing

Author: Miguel Menendez Alvarez

File organization:
- Code. 
	Directory with the program versions and binary files
	Each file contains the description of the algorithm used
- Data. 
	Directory with examples used for computation
- Report.
	Document with the analysis done
- Report data.
	Document with the data collected in the analysis
