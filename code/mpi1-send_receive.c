/**First MPI program using SPMD approach
* Author: Miguel Menendez Alvarez
*
*	Algorithm:
*  -----------------------------------------------------------------------
*		-Step 1 (twice, one per matrix):
*			* Process 0: read the matrix, send row chops to the rest.
*			* All computes their part and (not 0) send it to 0.
*			* Transpose it and process it again.
*			* Transpose it back
*
*		-Step 2:
*			* Process 0: send row chops of both matrices to MM point-wise.
*			* Send chunks to process 0.
*		
*		-Step 3:
*			* Same step 1 but with inverse fft
*
*		-Step 4:
*			* Write the output file
*
*/
#include <mpi.h>
#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
//Given code
typedef struct {float r; float i;} complex;
static complex ctmp;

#define C_SWAP(a,b) {ctmp=(a);(a)=(b);(b)=ctmp;}


//Main program constants
static const char INPUTFILENAME1[] = "../data/1_im1";
static const char INPUTFILENAME2[] = "../data/1_im2";
static const char OUTPUTFILENAME1[] = "../data/1_output";
#define N 512

//Function headers
complex **alloc_2d_float(int rows, int cols);
complex** transposeMatrix(complex **data, int n);
void c_fft1d(complex *r, int      n, int      isign);
void readInput(complex **data, const char* filename);
void writeOutput(complex **data, const char* filename);
void writeIntermediateOutput(complex **data, const char* filename);

//Main program
int main(int argc, char **argv){
	int my_rank, p;
 MPI_Init(&argc, &argv);
 MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
 MPI_Comm_size(MPI_COMM_WORLD, &p);

 double mpiend, mpistart = MPI_Wtime();
 complex **A, **B, **C, test;
 int i,j,rows = N/p, tag=0;

 MPI_Status status;

    //Create a complex type for MPI
 MPI_Datatype MPI_Complex;
 MPI_Type_contiguous(2,MPI_FLOAT,&MPI_Complex);
 MPI_Type_commit(&MPI_Complex);


 if (my_rank == 0){
		// printf("Punto 1 en process %d\n", my_rank);
	  	//Initialize matrix
  A=alloc_2d_float(N, N);
  B=alloc_2d_float(N, N);
  C=alloc_2d_float(N, N);
  for(i=0; i<N; i++)
   for(j=0; j<N; j++){
    A[i][j].r=0.0;A[i][j].i=0.0;
    B[i][j].r=0.0;B[i][j].i=0.0;
    C[i][j].r=0.0;C[i][j].i=0.0;
  }

	  	//Read matrix
  readInput(A, INPUTFILENAME1);
  readInput(B, INPUTFILENAME2);

  for(j=0; j<2; j++){//One time rows, one 'columns'
    	//Send chops
      tag++;
    	// printf("Punto 2 en process %d\n", my_rank);
      int lb;
      for(i=1; i<p; i++){
       lb = i * N/p;
       MPI_Send((void*)&A[lb][0], rows*N, MPI_Complex, i, tag, MPI_COMM_WORLD);
       MPI_Send((void*)&B[lb][0], rows*N, MPI_Complex, i, tag+1, MPI_COMM_WORLD);
     }
     tag++;
    // printf("Punto 3 en process %d\n", my_rank);
    //Compute a part
     for(i=0; i<N/p; i++){
       c_fft1d(&A[i][0], N, -1);
       c_fft1d(&B[i][0], N, -1);
     }
    		//Receive chops back
     tag++;
     for(i=1; i<p; i++){
       lb = i * N/p;
    			// printf("LB: %d\n", lb);
       MPI_Recv((void*)&A[lb][0], rows*N, MPI_Complex, i, tag, MPI_COMM_WORLD, &status);
     }
     tag++;
     for(i=1; i<p; i++){
      lb = i * N/p;
      MPI_Recv((void*)&B[lb][0], rows*N, MPI_Complex, i, tag, MPI_COMM_WORLD, &status);
    }
    A=transposeMatrix(A, N);
    B=transposeMatrix(B, N);
    
  }
}else{

  A = alloc_2d_float(rows,N);
  B = alloc_2d_float(rows,N);
  C = alloc_2d_float(rows,N);

  for(j=0; j<2; j++){//One time rows another 'columns'
    //Receive chops
    tag++;
    MPI_Recv((void*)&A[0][0], rows*N, MPI_Complex, 0, tag, MPI_COMM_WORLD, &status);
    tag++;
    MPI_Recv((void*)&B[0][0], rows*N, MPI_Complex, 0, tag, MPI_COMM_WORLD, &status);
    //Compute a part
    		// printf("Llegado %d, position 0,0: %f\n", my_rank, A[0][0].r);
    		// printf("Llegado %d, position rows-1,0: %f\n", my_rank, A[rows-1][N-1].r);
    for(i=0; i<N/p; i++)
       c_fft1d(&A[i][0], N, -1);
    //Send chops back
    tag++;
    MPI_Send((void*)&A[0][0], rows*N, MPI_Complex, 0, tag, MPI_COMM_WORLD);

    for(i=0; i<N/p; i++)
      c_fft1d(&B[i][0], N, -1);
    tag++;
    MPI_Send((void*)&B[0][0], rows*N, MPI_Complex, 0, tag, MPI_COMM_WORLD);
  }
}

//Product between A and B
if(my_rank == 0){
  //Send chops
  tag++;
        // printf("Punto 2 en process %d\n", my_rank);
  int lb;
  for(i=1; i<p; i++){
    lb = i * N/p;
    MPI_Send((void*)&A[lb][0], rows*N, MPI_Complex, i, tag, MPI_COMM_WORLD);
    MPI_Send((void*)&B[lb][0], rows*N, MPI_Complex, i, tag+1, MPI_COMM_WORLD);
  }
  tag++;
        // printf("Punto 3 en process %d\n", my_rank);
  //Compute a part
  for(i=0; i<N/p; i++)
    for(j=0; j<N; j++)
      C[i][j].r = A[i][j].r * B[i][j].r;

  tag++;
  for(i=1; i<p; i++){
    lb = i * N/p;
    MPI_Recv((void*)&C[lb][0], rows*N, MPI_Complex, i, tag, MPI_COMM_WORLD, &status);
  }


}else{
  tag++;
  MPI_Recv((void*)&A[0][0], rows*N, MPI_Complex, 0, tag, MPI_COMM_WORLD, &status);
  tag++;
  MPI_Recv((void*)&B[0][0], rows*N, MPI_Complex, 0, tag, MPI_COMM_WORLD, &status);

  //Compute a part
  for(i=0; i<N/p; i++)
    for(j=0; j<N; j++)
      C[i][j].r = A[i][j].r * B[i][j].r;

  tag++;
  MPI_Send((void*)&C[0][0], rows*N, MPI_Complex, 0, tag, MPI_COMM_WORLD);

}

//Inverse FFT of C to obtain the final output
if(my_rank == 0){
  for(j=0; j<2; j++){
    //Send chops
    tag++;
    // printf("Punto 2 en process %d\n", my_rank);
    int lb;
    for(i=1; i<p; i++){
      lb = i * N/p;
      MPI_Send((void*)&C[lb][0], rows*N, MPI_Complex, i, tag, MPI_COMM_WORLD);
    }
    //Compute a part
     for(i=0; i<N/p; i++){
       c_fft1d(&C[i][0], N, 1);
     }
        //Receive chops back
     tag++;
     for(i=1; i<p; i++){
       lb = i * N/p;
          // printf("LB: %d\n", lb);
       MPI_Recv((void*)&C[lb][0], rows*N, MPI_Complex, i, tag, MPI_COMM_WORLD, &status);
     }
     
     C=transposeMatrix(C, N);
  }
}else{
  for(j=0; j<2; j++){
    //Receive chops
    tag++;
    MPI_Recv((void*)&C[0][0], rows*N, MPI_Complex, 0, tag, MPI_COMM_WORLD, &status);
    //Compute a part
    for(i=0; i<N/p; i++)
       c_fft1d(&C[i][0], N, 1);
    //Send chops back
    tag++;
    MPI_Send((void*)&C[0][0], rows*N, MPI_Complex, 0, tag, MPI_COMM_WORLD);
  }
}

if(my_rank == 0){
  writeOutput(C, OUTPUTFILENAME1);
}

free(A[0]);
free(A);
free(B[0]);
free(B);
free(C[0]);
free(C);
MPI_Finalize();
return 0;
}


complex** transposeMatrix(complex **data, int n){
  complex **output = alloc_2d_float(n, n);
  int i,j;
  for(i=0; i<n; i++)
    for(j=0; j<n; j++)
      output[i][j] = data[j][i];

  // data = output;
  return output;
  }

//Read file function (given)
  void readInput(complex **data, const char* filename){
   int i,j;
	FILE *fp; /*open file descriptor */
   fp = fopen(filename, "r");
   for (i=0;i<N;i++)
    for (j=0;j<N;j++)
     fscanf(fp,"%g", &data[i][j].r);
   fclose(fp);
 }


//Write file function (given)
 void writeOutput(complex **data, const char* filename){
   int i,j;
	FILE *fp; /*open file descriptor */
   fp = fopen(filename, "w");
   for (i=0;i<N;i++) {
    for (j=0;j<N;j++)
     fprintf(fp,"%1.5g\t",data[i][j].r);
   fprintf(fp,"\n");
 }
 fclose(fp);
}

//Write file function (given)
 void writeIntermediateOutput(complex **data, const char* filename){
   int i,j;
  FILE *fp; /*open file descriptor */
   fp = fopen(filename, "w");
   for (i=0;i<N;i++) {
    for (j=0;j<N;j++){
     fprintf(fp,"%6.2g\t",data[i][j].r);
      fprintf(fp,"%6.2g\t",data[i][j].i);
    }
   fprintf(fp,"\n");
 }
 fclose(fp);
}

complex **alloc_2d_float(int rows, int cols) {
  complex *data = (complex*)malloc(rows*cols*sizeof(complex));
  complex **array= (complex**)malloc(rows*sizeof(complex*));
  int i;
  for (i=0; i<rows; i++)
    array[i] = &(data[cols*i]);

  return array;
}


/*
 ------------------------------------------------------------------------
 FFT1D            c_fft1d(r,i,-1)
 Inverse FFT1D    c_fft1d(r,i,+1)
 ------------------------------------------------------------------------
*/
/* ---------- FFT 1D
   This computes an in-place complex-to-complex FFT
   r is the real and imaginary arrays of n=2^m points.
   isign = -1 gives forward transform
   isign =  1 gives inverse transform
*/
   void c_fft1d(complex *r, int      n, int      isign)
   {
     int     m,i,i1,j,k,i2,l,l1,l2;
     float   c1,c2,z;
     complex t, u;

     if (isign == 0) return;

   /* Do the bit reversal */
     i2 = n >> 1;
     j = 0;
     for (i=0;i<n-1;i++) {
      if (i < j)
       C_SWAP(r[i], r[j]);
     k = i2;
     while (k <= j) {
       j -= k;
       k >>= 1;
     }
     j += k;
   }

   /* m = (int) log2((double)n); */
   for (i=n,m=0; i>1; m++,i/=2)
    ;

   /* Compute the FFT */
     c1 = -1.0;
   c2 =  0.0;
   l2 =  1;
   for (l=0;l<m;l++) {
    l1   = l2;
    l2 <<= 1;
    u.r = 1.0;
    u.i = 0.0;
    for (j=0;j<l1;j++) {
     for (i=j;i<n;i+=l2) {
      i1 = i + l1;

            /* t = u * r[i1] */
      t.r = u.r * r[i1].r - u.i * r[i1].i;
      t.i = u.r * r[i1].i + u.i * r[i1].r;

            /* r[i1] = r[i] - t */
      r[i1].r = r[i].r - t.r;
      r[i1].i = r[i].i - t.i;

            /* r[i] = r[i] + t */
      r[i].r += t.r;
      r[i].i += t.i;
    }
    z =  u.r * c1 - u.i * c2;

    u.i = u.r * c2 + u.i * c1;
    u.r = z;
  }
  c2 = sqrt((1.0 - c1) / 2.0);
      if (isign == -1) /* FWD FFT */
  c2 = -c2;
  c1 = sqrt((1.0 + c1) / 2.0);
}

   /* Scaling for inverse transform */
   if (isign == 1) {       /* IFFT*/
for (i=0;i<n;i++) {
 r[i].r /= n;
 r[i].i /= n;
}
}
}