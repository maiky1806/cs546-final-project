/**Fourth MPI program using task and data parallelism
* Author: Miguel Menendez Alvarez
*
*	Algorithm:
*  -----------------------------------------------------------------------
*		-Step 1:
*			* Process with last ID: read the matrix, send row A chops to P1 and B chops to P2.
*
*		-Step 2 P1 and P2 process A and B FFTs:
*     * P1 and P2 computes their part of A and B, and send it to the first member of its groups.
*			* The first member transpose it and distribute parts and process it again.
*			* The first member transpose it back and send it to group P3.
*
*		-Step 3 P3 multiply both:
*			* Each member receive part of A and B and computes the multiplication.
*			* Send chunks to P4.
*		
*		-Step 4 P4 process C inverse FFT:
*			* Same step 1 but with inverse fft
*
*		-Step 5:
*			* First member of P4 writes
*
*/
#include <mpi.h>
#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
//Given code
typedef struct {float r; float i;} complex;
static complex ctmp;

#define C_SWAP(a,b) {ctmp=(a);(a)=(b);(b)=ctmp;}


//Main program constants
static const char INPUTFILENAME1[] = "../data/1_im1";
static const char INPUTFILENAME2[] = "../data/1_im2";
static const char OUTPUTFILENAME1[] = "../data/1_output";
#define N 512

//Function headers
complex **alloc_2d_float(int rows, int cols);
complex** transposeMatrix(complex **data, int n);
void c_fft1d(complex *r, int      n, int      isign);
void readInput(complex **data, const char* filename);
void writeOutput(complex **data, const char* filename);

//Main program
int main(int argc, char **argv){
int my_rank, p;
 MPI_Init(&argc, &argv);
 MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
 MPI_Comm_size(MPI_COMM_WORLD, &p);

 double mpiend, mpistart = MPI_Wtime();
 complex **A, **B, **C, **D;
 int i,j,rows, tag=0;
int count;
 complex **localA, **localB, **localC, **localD;
 MPI_Status status;
int lb;
    //Create a complex type for MPI
 MPI_Datatype MPI_Complex;
 MPI_Type_contiguous(2,MPI_FLOAT,&MPI_Complex);
 MPI_Type_commit(&MPI_Complex);

//CHECK FOR PAIR NUMBER OF PROCESSORS
 int procXGroup=p/4;
 rows = N/procXGroup;
 if(p%4 != 0){
  MPI_Finalize();
}

//First part, 7 reads and send A:0,4 (P1) and B:1,3 (P2)
 if (my_rank == p-1){

	//Initialize matrix
  A=alloc_2d_float(N, N);
  B=alloc_2d_float(N, N);
  for(i=0; i<N; i++)
   for(j=0; j<N; j++){
    A[i][j].r=0.0;A[i][j].i=0.0;
    B[i][j].r=0.0;B[i][j].i=0.0;
  }
	//Read matrix
  readInput(A, INPUTFILENAME1);
  readInput(B, INPUTFILENAME2);
  j=0;
  for(i=0; i<procXGroup; i++){
       lb = i * rows;
       MPI_Send((void*)&A[lb][0], rows*N, MPI_Complex, j, 1, MPI_COMM_WORLD);
       MPI_Send((void*)&B[lb][0], rows*N, MPI_Complex, j+1, 2, MPI_COMM_WORLD);
       j+=4;
  }
}

//Group 1: FFT OF A
if(my_rank%4 == 0){
  //Only the first one of the group enter
  if(my_rank < 4){
    A=alloc_2d_float(N, N);
    for(i=0; i<N; i++)
       for(j=0; j<N; j++){
        A[i][j].r=0.0;A[i][j].i=0.0;}
    MPI_Recv((void*)&A[0][0], rows*N, MPI_Complex, p-1, 1, MPI_COMM_WORLD, &status);
    for(j=0; j<2; j++){
      //Compute a part
      for(i=0; i<rows; i++){
        c_fft1d(&A[i][0], N, -1);
      }
      count=0+4;
      for(i=1; i<procXGroup; i++){
        lb = i * rows;
        MPI_Recv((void*)&A[lb][0], rows*N, MPI_Complex, count, 10-j, MPI_COMM_WORLD, &status);
        count+=4;
      }
      A=transposeMatrix(A, N);
      if(j==0){
        count=0+4;
        for(i=1; i<procXGroup; i++){
          lb = i * rows;
          MPI_Send((void*)&A[lb][0], rows*N, MPI_Complex, count, 11+j, MPI_COMM_WORLD);
          count+=4;
        }
      }
    }
    //Send parts to the next group
    count=2;
    for(i=0; i<procXGroup; i++){
      lb = i * rows;
      MPI_Send((void*)&A[lb][0], rows*N, MPI_Complex, count, 30, MPI_COMM_WORLD);
      count+=4;
    }
  //Rest of the group
  }else{
    localA = alloc_2d_float(rows, N);
    MPI_Recv((void*)&localA[0][0], rows*N, MPI_Complex, p-1, 1, MPI_COMM_WORLD, &status);
    for(j=0;j<2;j++){
      //Compute a part
      for(i=0; i<rows; i++){
        c_fft1d(&localA[i][0], N, -1);
      }
      MPI_Send((void*)&localA[0][0], rows*N, MPI_Complex, 0, 10-j, MPI_COMM_WORLD);
      if (j==0){
        MPI_Recv((void*)&localA[0][0], rows*N, MPI_Complex, 0, 11+j, MPI_COMM_WORLD, &status);
      }
    }
  }
    
}


//Group 2: FFT OF B
if(my_rank%4 == 1){
  //Only the first one of the group enter
  if(my_rank < 4){
    B=alloc_2d_float(N, N);
    for(i=0; i<N; i++)
       for(j=0; j<N; j++){
        B[i][j].r=0.0;B[i][j].i=0.0;}

    MPI_Recv((void*)&B[0][0], rows*N, MPI_Complex, p-1, 2, MPI_COMM_WORLD, &status);
    for(j=0; j<2; j++){
      //Compute a part
      for(i=0; i<rows; i++){
        c_fft1d(&B[i][0], N, -1);
      }

      count=1+4;
      for(i=1; i<procXGroup; i++){
        lb = i * rows;
        MPI_Recv((void*)&B[lb][0], rows*N, MPI_Complex, count, 20, MPI_COMM_WORLD, &status);
        count+=4;
      }
      B=transposeMatrix(B, N);
      if(j==0){
        count=1+4;
        for(i=1; i<procXGroup; i++){
          lb = i * rows;
          MPI_Send((void*)&B[lb][0], rows*N, MPI_Complex, count, 21, MPI_COMM_WORLD);
          count+=4;
        }
      }
    }

    //Send parts to the next group
    count=2;
    for(i=0; i<procXGroup; i++){
      lb = i * rows;
      MPI_Send((void*)&B[lb][0], rows*N, MPI_Complex, count, 31, MPI_COMM_WORLD);
      count+=4;
    }


  //Rest of the group
  }else{
    localB = alloc_2d_float(rows, N);
    MPI_Recv((void*)&localB[0][0], rows*N, MPI_Complex, p-1, 2, MPI_COMM_WORLD, &status);
    for(j=0;j<2;j++){
      //Compute a part
      for(i=0; i<rows; i++){
        c_fft1d(&localB[i][0], N, -1);
      }
      MPI_Send((void*)&localB[0][0], rows*N, MPI_Complex, 1, 20, MPI_COMM_WORLD);
      if (j==0)
        MPI_Recv((void*)&localB[0][0], rows*N, MPI_Complex, 1, 21, MPI_COMM_WORLD, &status);
    }
  }  
    
}


//GROUP 3: Product between A and B
if(my_rank%4 == 2){
  localA = alloc_2d_float(rows,N);
  localB = alloc_2d_float(rows,N);
  localC = alloc_2d_float(rows,N);
  MPI_Recv((void*)&localA[0][0], rows*N, MPI_Complex, 0, 30, MPI_COMM_WORLD, &status);
  MPI_Recv((void*)&localB[0][0], rows*N, MPI_Complex, 1, 31, MPI_COMM_WORLD, &status);
  
  //Compute a part
  for(i=0; i<rows; i++)
    for(j=0; j<N; j++){
      localC[i][j].r = localA[i][j].r * localB[i][j].r;}

    //Send to next group
    MPI_Send((void*)&localC[0][0], rows*N, MPI_Complex, my_rank+1, 40, MPI_COMM_WORLD);
}



//Group 4: Inverse FFT of C to obtain the final output
if(my_rank%4 == 3){
  //Only the first one of the group enter
  if(my_rank < 4){
    D=alloc_2d_float(N, N);
    for(i=0; i<N; i++)
       for(j=0; j<N; j++){
        D[i][j].r=0.0;D[i][j].i=0.0;}

    MPI_Recv((void*)&D[0][0], rows*N, MPI_Complex, my_rank-1, 40, MPI_COMM_WORLD, &status);
    for(j=0; j<2; j++){
      //Compute a part
      for(i=0; i<rows; i++){
        c_fft1d(&D[i][0], N, 1);
      }

      count=3+4;
      for(i=1; i<procXGroup; i++){
        lb = i * rows;
        MPI_Recv((void*)&D[lb][0], rows*N, MPI_Complex, count, 41, MPI_COMM_WORLD, &status);
        count+=4;
      }
      D=transposeMatrix(D, N);
      if(j==0){
        count=3+4;
        for(i=1; i<procXGroup; i++){
          lb = i * rows;
          MPI_Send((void*)&D[lb][0], rows*N, MPI_Complex, count, 42, MPI_COMM_WORLD);
          count+=4;
        }
      }
    }
    writeOutput(D, OUTPUTFILENAME1);


  //Rest of the group
  }else{
    localD = alloc_2d_float(rows, N);
    MPI_Recv((void*)&localD[0][0], rows*N, MPI_Complex, my_rank-1, 40, MPI_COMM_WORLD, &status);
    for(j=0;j<2;j++){
      //Compute a part
      for(i=0; i<rows; i++){
        c_fft1d(&localD[i][0], N, 1);
      }
      MPI_Send((void*)&localD[0][0], rows*N, MPI_Complex, 3, 41, MPI_COMM_WORLD);
      if (j==0)
        MPI_Recv((void*)&localD[0][0], rows*N, MPI_Complex, 3, 42, MPI_COMM_WORLD, &status);
    }
  }  
    
}

MPI_Finalize();
return 0;
}


complex** transposeMatrix(complex **data, int n){
  complex **output = alloc_2d_float(n, n);
  int i,j;
  for(i=0; i<n; i++)
    for(j=0; j<n; j++)
      output[i][j] = data[j][i];

  return output;
  }

//Read file function (given)
  void readInput(complex **data, const char* filename){
   int i,j;
	FILE *fp; /*open file descriptor */
   fp = fopen(filename, "r");
   for (i=0;i<512;i++)
    for (j=0;j<512;j++)
     fscanf(fp,"%g", &data[i][j].r);
   fclose(fp);
 }


//Write file function (given)
 void writeOutput(complex **data, const char* filename){
   int i,j;
	FILE *fp; /*open file descriptor */
   fp = fopen(filename, "w");
   for (i=0;i<512;i++) {
    for (j=0;j<512;j++)
     fprintf(fp,"%6.2g\t",data[i][j].r);
   fprintf(fp,"\n");
 }
 fclose(fp);
}

complex **alloc_2d_float(int rows, int cols) {
  complex *data = (complex*)malloc(rows*cols*sizeof(complex));
  complex **array= (complex**)malloc(rows*sizeof(complex*));
  int i;
  for (i=0; i<rows; i++)
    array[i] = &(data[cols*i]);

  return array;
}


/*
 ------------------------------------------------------------------------
 FFT1D            c_fft1d(r,i,-1)
 Inverse FFT1D    c_fft1d(r,i,+1)
 ------------------------------------------------------------------------
*/
/* ---------- FFT 1D
   This computes an in-place complex-to-complex FFT
   r is the real and imaginary arrays of n=2^m points.
   isign = -1 gives forward transform
   isign =  1 gives inverse transform
*/
   void c_fft1d(complex *r, int      n, int      isign)
   {
     int     m,i,i1,j,k,i2,l,l1,l2;
     float   c1,c2,z;
     complex t, u;

     if (isign == 0) return;

   /* Do the bit reversal */
     i2 = n >> 1;
     j = 0;
     for (i=0;i<n-1;i++) {
      if (i < j)
       C_SWAP(r[i], r[j]);
     k = i2;
     while (k <= j) {
       j -= k;
       k >>= 1;
     }
     j += k;
   }

   /* m = (int) log2((double)n); */
   for (i=n,m=0; i>1; m++,i/=2)
    ;

   /* Compute the FFT */
     c1 = -1.0;
   c2 =  0.0;
   l2 =  1;
   for (l=0;l<m;l++) {
    l1   = l2;
    l2 <<= 1;
    u.r = 1.0;
    u.i = 0.0;
    for (j=0;j<l1;j++) {
     for (i=j;i<n;i+=l2) {
      i1 = i + l1;

            /* t = u * r[i1] */
      t.r = u.r * r[i1].r - u.i * r[i1].i;
      t.i = u.r * r[i1].i + u.i * r[i1].r;

            /* r[i1] = r[i] - t */
      r[i1].r = r[i].r - t.r;
      r[i1].i = r[i].i - t.i;

            /* r[i] = r[i] + t */
      r[i].r += t.r;
      r[i].i += t.i;
    }
    z =  u.r * c1 - u.i * c2;

    u.i = u.r * c2 + u.i * c1;
    u.r = z;
  }
  c2 = sqrt((1.0 - c1) / 2.0);
      if (isign == -1) /* FWD FFT */
  c2 = -c2;
  c1 = sqrt((1.0 + c1) / 2.0);
}

   /* Scaling for inverse transform */
   if (isign == 1) {       /* IFFT*/
for (i=0;i<n;i++) {
 r[i].r /= n;
 r[i].i /= n;
}
}
}